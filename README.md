# ArmingVim
This is my preferred vim configuration and plugins.


**1) Install required packages:**
```
sudo apt update
sudo apt install curl git ctags
```


**2) Clone configs from repository:**
```
git clone https://github.com/imto1/ArmingVim.git
```


### Use installer script
**3) Open terminal and run install.sh script:**
```
./install.sh
```
Now move to level **4**.


### Or do it yourself
**3-1) Set config files in place:**
```
mv vim ~/.vim
mv ~/.vim/vimrc ~/.vimrc
cd ..
rm -rf ./ArmingVim
```


**3-2) Install _Vim-Plug_ plugin manager:**
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```


**4) Open Vim and ignore Vim errors on first launch. After Vim opened, run PlugInstall command:**
```
:PlugInstall
```
It's done!

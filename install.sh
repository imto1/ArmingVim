#!/bin/bash
mv vim ~/.vim
mv ~/.vim/vimrc ~/.vimrc
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cd ..
rm -rf ./ArmingVim